from dateutil import parser
from functools import total_ordering
from operator import getitem
# from sys import argv
from datetime import datetime, timedelta, date
import pytz
import paho.mqtt.client as mqtt
from requests import get
import yaml
from json import load, dump
from os import system
from time import sleep

print("??")

def sub_cb(client, userdata, msg):
    print("!")
    theme = msg.payload
    print(theme)
    col = get_lamp_colors(theme)
    client.publish("circadia/data", col)
    return


client = mqtt.Client("circadia")
client.connect('10.0.0.240')
client.message_callback_add("circadia/request", sub_cb)
client.subscribe("circadia/request")
client.loop_start()


def get_day_data():
    # Save day data to local file, named by date
    # If file exists, don't call API again
    # If it doesn't exist, wipe the dir first
    today = date.today()
    data_file = f"day_data/{today}.json"
    try:
        with open(data_file, 'r') as day_data_file:
            day_data = load(day_data_file)
            return day_data
    except:
        system("rm -f ./day_data/*.json")
        lat = "37.784713"
        lon = "-122.460310"
        day_data = get(f"https://api.sunrise-sunset.org/json?lat={lat}&lng={lon}&formatted=0").json()
        with open(data_file, 'w') as day_data_file:
            dump(day_data, day_data_file)
        return day_data

def end_of_period(mode_data, current_mode=None):
    if current_mode:
        return max(mode_data[current_mode].keys())
    return max(mode_data.keys())
def start_of_period(mode_data, current_mode=None):
    if current_mode:
        return min(mode_data[current_mode].keys())
    return min(mode_data.keys())

def bracketing_times(times, t):
    ordered = sorted(list(times))
    for i, o in enumerate(ordered):
        if o >= t:
            u = o
            l = ordered[i-1]
            return l, u
    print("oops")
    return None

def linear_blend(x, l, u):
    if l == u:
        return l
    b = l + ((u - l) * x)
    return b

def rgbcwd_blend(x, l, u):
    rgbcwd = (
        int(linear_blend(x, l[0], u[0])),
        int(linear_blend(x, l[1], u[1])),
        int(linear_blend(x, l[2], u[2])),
        int(linear_blend(x, l[3], u[3])),
        int(linear_blend(x, l[4], u[4])),
        int(linear_blend(x, l[5], u[5])),
    )
    return rgbcwd

def get_time_ratio(time_now, period_start, period_end):
    time_from_start = (time_now - period_start).seconds
    total_time = (period_end - period_start).seconds
    time_ratio = time_from_start/total_time
    print(f"Time Ratio {time_ratio} between | {period_start} | {period_end} |")
    return time_ratio


def get_color_for_time(mode_data, time_ratio):
    start_of_mode = start_of_period(mode_data)
    end_of_mode = end_of_period(mode_data)
    duration_of_mode = end_of_mode - start_of_mode
    # print(f"start mode {start_of_mode}")
    # print(f"end mode {end_of_mode}")
    # print(f"duration_of_mode {duration_of_mode}")
    # How far through this mode are we
    mode_ratio = (time_ratio * duration_of_mode) + start_of_mode
    print(f"mode ratio {mode_ratio}")
    # Which two defined colors are either side of this time
    lower_time, upper_time = bracketing_times(mode_data.keys(), mode_ratio)
    print(f"lower_time {lower_time} upper_time {upper_time}")
    # How far between the two colors/times are we
    if upper_time == lower_time:
        blend_ratio = 1
    else:
        blend_ratio = ((mode_ratio -lower_time) / (upper_time - lower_time))
    print(blend_ratio)
    # Blend colors from each time according to how far through we are
    col = rgbcwd_blend(blend_ratio, mode_data[lower_time], mode_data[upper_time])
    return col

def get_lamp_colors(theme='theme1'):
    day_data = get_day_data()
    sunrise_time = day_data['results']['sunrise']
    sunset_time = day_data['results']['sunset']
    theme = "theme1"
    theme_path = f"./{theme}.yaml"
    print(theme_path)
    theme_data = yaml.safe_load(open(theme_path, 'r'))
    time_now = datetime.now().astimezone(pytz.timezone('US/Pacific'))

    # Sunrise is gradual brightening to help with waking up
    # Sunset is more of a pretty effect
    # Gradual evening/evening mode is used for help sleeping
    # Get start and end of defined sunrise and sunset periods from theme
    # Day mode is any time between sunrise and sunset - should have a gradual transition between last sunrise color>day>first sunset color

    # Time of exact sunrise/sunset
    # sunrise_time = datetime.strptime(sunrise_time, '%Y-%m-%dT%H:%M:%S%z').astimezone(pytz.timezone('US/Pacific'))
    # sunset_time = datetime.strptime(sunset_time,  '%Y-%m-%dT%H:%M:%S%z').astimezone(pytz.timezone('US/Pacific'))
    sunrise_time = parser.parse(sunrise_time).astimezone(pytz.timezone('US/Pacific'))
    sunset_time = parser.parse(sunset_time).astimezone(pytz.timezone('US/Pacific'))

    # Timestamps of start and end of sunrise/sunset transitions according to theme
    # Day, evening, night/dawn fit dynamically between these
    sunrise_start_time = timedelta(minutes=start_of_period(theme_data, 'sunrise')) + sunrise_time
    sunrise_end_time   = timedelta(minutes=end_of_period(theme_data, 'sunrise')) + sunrise_time
    sunset_start_time = timedelta(minutes=start_of_period(theme_data, 'sunset')) + sunset_time
    sunset_end_time   = timedelta(minutes=end_of_period(theme_data, 'sunset')) + sunset_time
    midnight = datetime.now().replace(hour=11, minute=59, second=59).astimezone(pytz.timezone('US/Pacific'))

    if time_now <= sunrise_start_time:
        # current_mode = "dawn"
        current_mode = "night"
    elif time_now <= sunrise_end_time:
        current_mode = "sunrise"
    elif time_now <= sunset_start_time:
        current_mode = "day"
    elif time_now <= sunset_end_time:
        current_mode = "sunset"
    elif time_now <= midnight:
        current_mode = "evening"
    else:
        current_mode = "night"

    print(current_mode)
    mode_data = theme_data[current_mode]
    # print(mode_data)

    # TODO handle delayed sunrise
    # Could this just be a matter of having really offset theme times, eg from 60 to 180

    # Find start and end times of this period
    if current_mode == 'day':
        period_start = sunrise_end_time
        period_end = sunset_start_time
    elif current_mode == 'sunrise':    
        period_start = sunrise_start_time
        period_end = sunrise_end_time
    elif current_mode == 'sunset':
        period_start = sunset_start_time
        period_end = sunset_end_time
    elif current_mode == 'evening':
        period_start = sunset_end_time
        period_end = midnight
    elif current_mode == 'night':
        period_start = midnight
        period_end = sunrise_start_time

    time_ratio = get_time_ratio(time_now, period_start, period_end)
    col = get_color_for_time(mode_data, time_ratio)
    col = str(col).replace('(','').replace(')','').replace(' ', '')
    print(col)
    return col
# def send_to_lights(rgbcwd):
#     # TODO store previous sends, to avoid retransmitting too often
#     devices = 'hue1 hue2 hue3'.split(' ')
#     topic = "cmnd/{device}/{command}"
#     dimmer = rgbcwd[5]
#     color = rgbcwd[0:4]
#     for device in devices:
#         # client.publish(topic.format(device, 'speed'), speed)
#         client.publish(topic.format(device, 'dimmer'), dimmer)
#         # Can also ignore dimmer and use color1
#         client.publish(topic.format(device, 'color2'), color)

print("started")
print(get_lamp_colors())
while True:
    sleep(1)