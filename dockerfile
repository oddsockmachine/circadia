FROM python:3.6.5-slim-stretch

WORKDIR /usr/src/app

COPY requirements.txt ./
COPY theme1.yaml ./
COPY day_data/ ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", "./circadia.py" ]
