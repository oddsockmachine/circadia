New Sunrise:

Some number of RGBCW bulbs in fixtures
    Fixtures should eventually be cool, and integrated (ie not just lamo stands)
    Could apply this to whole house, not just bedroom, for full day circadian rhythm support

Sunrise mode:
    Run through color gradient during the morning
    Starting at appropriate time based on previous bedtime, sunrise time, or alarm time
    Start very dim and warm, sunrise colors, then get brighter and colder

Sunset mode:
    Run through sunset gradient in evening
    Only if I'm home, and other lights aren't on yet
    Start bright, sunset, gradually dim and warm?

Lamp mode:
    Choose correct color temperature based on time of day (and weather?)

Maybe we're just always in lamp mode, and the color/temp/gradient is selected automatically 


Can all this be done in NodeRed, or is a separate processor needed?
Think a python script could be used to calculate appropriate color temps for a given time/theme
Can that be a standalone script called by NR - a cron job that updates a file/env var - or a service which gets called over the network?

Start with a callable script, can always be made a service later if needed. But for now, easier to develop and chain up to mqtt

Input:
    Theme (path to yaml file)
    Time (or use current time, assuming TZs are correct)
    Alarm Time (if using)
    Bedtime (if using)
        Maybe nodered should decide upon bedtime, alarm time, etc, and input here is just when we expect the middle to be
    Number of lights (from 1-n, since we may want primary/secondary/more colors)
Output:
    Array of RGBCW values

Theme Data:



mosquitto_pub -h jarvis.local -t cmnd/hue2/Dimmer -m 100
mosquitto_pub -h jarvis.local -t cmnd/hue1/Dimmer -m 100
mosquitto_pub -h jarvis.local -t cmnd/hue3/Dimmer -m 100


mosquitto_pub -h jarvis.local -t cmnd/hue2/Dimmer -m 0
mosquitto_pub -h jarvis.local -t cmnd/hue1/Dimmer -m 0
mosquitto_pub -h jarvis.local -t cmnd/hue3/Dimmer -m 0


mosquitto_pub -h jarvis.local -t cmnd/hue2/CT -m 500
mosquitto_pub -h jarvis.local -t cmnd/hue1/CT -m 500
mosquitto_pub -h jarvis.local -t cmnd/hue3/CT -m 500